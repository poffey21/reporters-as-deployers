FROM python:slim

ENV PROJECT=report-as-deployers
ENV CONTAINER_HOME=/srv
ENV CONTAINER_PROJECT=$CONTAINER_HOME/$PROJECT


COPY requirements.txt $CONTAINER_PROJECT/requirements.txt
RUN pip install --no-cache-dir -r $CONTAINER_PROJECT/requirements.txt

COPY protect.py $CONTAINER_PROJECT
