# Reporters as Deployers

Given a GitLab Group, easily grant that group the ability to deploy to an environment within a project.

This project is aiming at automating the [Configuring of a Protected Environment](https://docs.gitlab.com/ee/ci/environments/protected_environments.html#use-the-api-to-protect-an-environment) without the need to interact with the GitLab API.

## Getting started

1. Fork this project or clone it to the location in which you want to configure.
1. Go to `CI/CD` > `Pipelines` > `Run pipeline` (top right)
1. Fill in the details 
1. Submit the pipeline by clicking `Run pipeline` and it ought to configure a protected environment for you.
