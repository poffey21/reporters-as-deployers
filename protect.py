from envparse import Env
from urllib.parse import quote_plus
from gitlab import Gitlab as GitLab
import json
import requests
import os
import sys


env = Env()
if os.path.isfile('.env'):
    env.read_envfile('.env')

REPORTER_ACCESS_LEVEL = 20

def main():
    private_token = env('PRIVATE_TOKEN')
    origin = env('GITLAB_HOST')
    print(f'Connecting to: {origin}')
    if 'api/v4' in origin:
        print('Do not include api/v4 in the GitLab Host')
        exit(1)
    if 'https://' not in origin and 'http://' not in origin:
        print('include the https:// part of the address')
        exit(1)
    if origin.endswith('/'):
        origin = origin[:-1]
    debug = env('DEBUG', cast=bool, default=False)
    gl = GitLab(url=origin, private_token=private_token)
    headers = {
        'PRIVATE-TOKEN': private_token
    }
    gl.auth()

    group_namespace = quote_plus(env('DEPLOYMENT_GROUP_NAMESPACE'))
    url = f'{origin}/api/v4/groups/{group_namespace}'
    # group_id = gl.groups.get(group_namespace).id
    group_id = requests.get(url=url, headers=headers).json()['id']
    print(f'Retrieved Group ID: {group_id}')
    project_namespace = quote_plus(env('PROJECT_NAMESPACE'))
    # project_id = gl.projects.get(project_namespace).id
    url = f'{origin}/api/v4/projects/{project_namespace}'
    project_id = requests.get(url=url, headers=headers).json()['id']
    print(f'Retrieved Project ID: {project_id}')

    print('Adding users to group.')
    for username in env('DEPLOYMENT_GROUP_USERS', cast=list):
        try:
            user_id = int(username)
        except ValueError:
            users = gl.users.list(username=username)
            if len(users) > 0:
                user_id = users[0].id
            else:
                print(f'could not find a user for: {username}')
                exit(1)
        url = f'{origin}/api/v4/groups/{group_id}/members'
        data = {
            'user_id': user_id,
            'access_level': REPORTER_ACCESS_LEVEL
        }

        resp = requests.post(
            url=url,
            headers=headers,
            json=data,
        )
        if not resp.ok and resp.status_code != 409:
            print(resp.json()['message'])
            exit(1)
    print('')

    ### 
    print('Adding the group to the project as a reporter')
    url = f'{origin}/api/v4/projects/{project_id}/share?group_id={group_id}&group_access={REPORTER_ACCESS_LEVEL}'
    resp = requests.post(
        url=url,
        headers=headers,
    )
    print('')
    if not resp.ok and resp.status_code != 409:
        print(resp.json()['message'])
        exit(1)

    ###
    print('Add the group with protected environment access')

    url = f'{origin}/api/v4/projects/{project_id}/protected_environments'
    data = {
        'name': env('PROJECT_ENVIRONMENT'),
        'deploy_access_levels': [{
            'group_id': group_id
        }]
    }

    resp = requests.post(
        url=url,
        headers=headers,
        json=data,
    )
    if not resp.ok and resp.status_code != 409:
        print(resp.json()['message'])
        exit(1)
    

    


if __name__ == "__main__":
    main()
